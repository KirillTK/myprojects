let slideIndex = 1;

const information = [{
  title: 'EMAIL TIP OF THE DAY 1',
  desctiption: 'Lorem ipsum dolor sit amet, eos nostrud persequeris at. Nostrum suscipit in eum, voluptaria persequeris sea an, est eu inani qualisque.',
}, {
  title: 'EMAIL TIP OF THE DAY 2',
  desctiption: 'Lorem ipsum dolor sit amet, eos nostrud persequeris at. Nostrum suscipit in eum.',
}, {
  title: 'EMAIL TIP OF THE DAY 3',
  desctiption: 'Lorem ipsum dolor sit amet, eos nostrud persequeris at.',
}, {
  title: 'EMAIL TIP OF THE DAY 4',
  desctiption: 'Lorem ipsum dolor sit amet, eos.',
}];

const generateHtmlComponent = '<div class="notification-container">\n'
    + '    <section class="notification">\n'
    + '      <div class="slider">\n'
    + '        <div class="slideshow-container"></div>\n'
    + '        <div class="navigation" style="text-align:center">\n'
    + '\n'
    + '          <label>\n'
    + '            <input type="checkbox" id="box-1">\n'
    + '            <label for="box-1">Disable Tips</label>\n'
    + '          </label>\n'
    + '          <div>\n'
    + '            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>\n'
    + '            <span class="dot" onclick="currentSlide(1)">1</span>\n'
    + '            <span class="dot" onclick="currentSlide(2)">2</span>\n'
    + '            <span class="dot" onclick="currentSlide(3)">3</span>\n'
    + '            <span class="dot" onclick="currentSlide(4)">4</span>\n'
    + '            <a class="next" onclick="plusSlides(1)">&#10095;</a>\n'
    + '          </div>\n'
    + '        </div>\n'
    + '      </div>\n'
    + '\n'
    + '\n'
    + '      <div class="close-btn">\n'
    + '        <button>&times</button>\n'
    + '      </div>\n'
    + '    </section>\n'
    + '  </div>';

function showSlides(n) {
  const slides = document.getElementsByClassName('mySlides');
  const dots = document.getElementsByClassName('dot');
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  Array.from(slides).forEach((slide) => { slide.style.display = 'none'; });
  Array.from(dots).forEach((dot) => { dot.className = dot.className.replace(' active', ''); });
  slides[slideIndex - 1].style.display = 'block';
  if (dots[slideIndex - 1]) {
    dots[slideIndex - 1].className += ' active';
  }
}

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function closeNotification() {
  const notification = document.querySelector('.notification-container');
  notification.style.display = 'none';
}

window.addEventListener('keydown', (e) => {
  if (e.keyCode === 37) {
    plusSlides(-1);
  }

  if (e.keyCode === 39) {
    plusSlides(+1);
  }

  if (e.keyCode === 27) {
    closeNotification();
  }
}, false);

function generateComponent() {
  document.getElementsByTagName('body')[0].innerHTML = generateHtmlComponent + document.getElementsByTagName('body')[0].innerHTML;
  document.querySelector('.close-btn button').onclick = closeNotification;
  document.querySelector('input[type="checkbox"]').onchange = (e) => {
    window.localStorage.setItem('Tips', e.target.checked);
  };
}

function generateElemenstsComponent() {
  const div = document.querySelector('.slideshow-container');
  information.forEach((value) => {
    const element = document.createElement('div');
    element.className = 'mySlides fade';
    element.innerHTML = `<h3>${value.title}</h3> <p>${value.desctiption}</p>`;
    div.appendChild(element);
  });
  currentSlide(slideIndex);
}

document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    if (window.localStorage.getItem('Tips') === 'false' || window.localStorage.getItem('Tips') === null) {
      generateComponent();
      const notification = document.querySelector('.notification-container');
      notification.style.display = 'block';
      generateElemenstsComponent();
    }
  }, 5000);
});
