import Slider from '../src/slider';
import generateInitialHtml from '../src/generateHtml';
import Loader from '../src/loader';
import YouTube from '../src/youtube';


test('generate html test', () => {
  const initHtml = '<div class="container">\n'
    + '  <h1>YouTube client</h1>\n'
    + '  <form>\n'
    + '    <div class="search search-input">\n'
    + '      <div class="smart-search"><input type="text" class="search" id="input" placeholder="&nbsp;"><i class="fas fa-search"></i>\n'
    + '      </div>\n'
    + '      <span class="border"></span>\n'
    + '    </div>\n'
    + '  </form>\n'
    + '\n'
    + '  <div class="cards">\n'
    + '  </div>\n'
    + '</div>';

  generateInitialHtml();
  expect(document.body.innerHTML).toBe(initHtml);
});

test('generate slider', () => {
  Slider.generateSlider();
  const cards = document.getElementsByClassName('cards')[0];
  const container = document.getElementsByClassName('container')[0];
  expect(cards.childElementCount).toBeGreaterThan(0);
  expect(container.lastElementChild.classList[0]).toBe('navigation');
});

test('test event on findVideo', () => {
  const findBtn = document.querySelector('.fa-search');
  expect(typeof findBtn.click).toBe('function');
});

test('test add new page to slider', () => {
  Slider.addNewPage();
  const cards = document.getElementsByClassName('cards')[0];
  expect(cards.childElementCount).toBe(2);
});


test('test getCurrentPosition Slider', () => {
  const number = Slider.getCurrntSliderPosition();
  expect(number).toBe(1);
});

test('calculate max elements in slider', () => {
  const count = Slider.getMaxCountElements();
  expect(typeof count).toBe('number');
});

test('test loader', () => {
  Loader.inserLoader();
  const loader = document.querySelector('.loader-container');
  expect(loader).not.toBeNull();
});

test('test convert date', () => {
  let date = YouTube.convertDate('2018-11-03T07:30:00.000Z');
  expect(date).toBe('2018-11-03');
  date = YouTube.convertDate('2017-11-03T07:30:00.000Z');
  expect(date).toBe('2017-11-03');
  date = YouTube.convertDate('2015-12-05T14:00:01.000Z');
  expect(date).toBe('2015-12-05');
});


test('clear slider', () => {
  Slider.clearHtml();
  const cardsHtml = document.querySelector('.cards').innerHTML;
  expect(cardsHtml).toBe('');
  const navigation = document.querySelector('.navigation');
  expect(navigation).toBeNull();
});
