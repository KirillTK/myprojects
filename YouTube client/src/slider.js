const sliderHtml = '<div class="mySlides fade"><div class="video-container"></div></div>';
const navigationHtml = '<div class="navigation" style="text-align:center"><div class="navigation-page"></div></div>';
const prevArrow = '<a class="prev">&#10094;</a>';
const nextArrow = '<a class="next">&#10095;</a>';
let slideIndex = 1;
let touchstartX = 0;
let touchendX = 0;


class Slider {
  static generateSlider() {
    const cards = document.getElementsByClassName('cards');
    cards[0].innerHTML += sliderHtml;
    cards[0].insertAdjacentHTML('afterend', navigationHtml);
  }

  static addNewNavigationItem(length, maxElem) {
    const count = Math.round(length / maxElem);
    const navigationPage = document.getElementsByClassName('navigation-page')[0];
    for (let i = 1; i <= count; i += 1) {
      navigationPage.innerHTML += `<span class="dot">${i}</span>`;
    }
    const spans = document.querySelectorAll('.dot');
    spans.forEach((span, index) => span.addEventListener('click', () => {
      this.currentSlide(index + 1);
    }));
    const navigation = document.getElementsByClassName('navigation')[0];
    navigation.insertAdjacentHTML('afterbegin', prevArrow);
    navigation.insertAdjacentHTML('beforeend', nextArrow);
    Slider.showSlides(slideIndex);
  }

  static addNewPage() {
    const cards = document.getElementsByClassName('cards');
    cards[0].insertAdjacentHTML('beforeend', sliderHtml);
  }

  static showSlides(position) {
    const slides = document.getElementsByClassName('mySlides');
    const span = document.getElementsByClassName('dot');
    if (position > slides.length) {
      slideIndex = 1;
    }
    if (position < 1 || position === 0) {
      slideIndex = slides.length;
    }
    for (let i = 0; i < slides.length; i += 1) {
      slides[i].style.display = 'none';
    }
    for (let i = 0; i < span.length; i += 1) {
      span[i].className = span[i].className.replace(' active', '');
    }
    slides[slideIndex - 1].style.display = 'block';
    span[slideIndex - 1].className += ' active';
  }

  static currentSlide(position) {
    this.showSlides(slideIndex = position);
  }

  static getCurrntSliderPosition() {
    return slideIndex;
  }

  static plusSlides(number) {
    this.currentSlide(slideIndex += number);
  }

  static addEvents(loadMore) {
    const slides = document.querySelectorAll('.mySlides');

    function checkLoad() {
      if (slides.length === slideIndex) {
        loadMore();
      }
    }

    function handleGesure() {
      if (touchendX < touchstartX) {
        Slider.plusSlides(1);
        checkLoad();
      }
      if (touchendX > touchstartX) {
        Slider.plusSlides(-1);
        checkLoad();
      }
    }

    slides.forEach((slide) => {
      slide.addEventListener('touchstart', (event) => {
        touchstartX = event.touches[0].clientX;
      }, false);

      slide.addEventListener('touchend', (event) => {
        touchendX = event.changedTouches[0].clientX;
        handleGesure();
      }, false);

      slide.addEventListener('mousedown', (event) => {
        touchstartX = event.clientX;
      });

      slide.addEventListener('mousemove', (event) => {
        touchendX = event.clientX;
      });

      slide.addEventListener('dragstart', (event) => {
        event.preventDefault();
      });

      slide.addEventListener('mouseup', (event) => {
        touchendX = event.clientX;
        handleGesure();
      });
    });

    document.querySelector('.prev').addEventListener('click', () => {
      this.plusSlides(-1);
      checkLoad();
    });

    document.querySelector('.next').addEventListener('click', () => {
      this.plusSlides(1);
      checkLoad();
    });
  }

  static getMaxCountElements() {
    const div = document.querySelector('.cards');
    const countElements = Math.floor(div.clientWidth / 400);
    return countElements >= 4 ? 4 : countElements;
  }

  static clearHtml() {
    document.querySelector('.cards').innerHTML = '';
    const navigation = document.querySelector('.navigation');
    if (navigation) navigation.remove();
  }
}


export default Slider;
