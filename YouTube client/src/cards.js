import Slider from './slider';

let loadMoreFn;

class Cards {
  static appendCards(youtubeVideos, countElements = 4, loadMore) {
    const sliderPosition = Slider.getCurrntSliderPosition();
    loadMoreFn = loadMore;
    let indexContainer = 0;
    Slider.clearHtml();
    Slider.generateSlider();
    Slider.addNewNavigationItem(youtubeVideos.length, countElements);
    youtubeVideos.forEach((video, index) => {
      const card = `${`${`${'<div class="item-video">\n'
        + '        <div class="video" style="background: url('}${video.image.medium.url}) no-repeat 50% 50%; background-size: cover"> <a href="https://www.youtube.com/watch?v=${video.id}" class="title-link" target="_blank">${video.title}</a> </div>\n`
        + '        <div class="statistics">\n'
        + '          <i class="fas fa-user"></i> <span class="user-name">'}${video.channelTitle}</span>\n`
        + `          <i class="fas fa-calendar-alt"></i> <span class="user-name">${video.publishedAt}</span>\n`
        + '          <i class="fas fa-eye"></i><span class="user-name">'}${video.viewCount}</span>\n`
        + '        </div>\n'
        + `        <p>${video.description}</p>\n`
        + '      </div>\n'
        + '    </div>';

      document.querySelectorAll('.video-container')[indexContainer].innerHTML += card;
      if ((index + 1) % countElements === 0 && (index + 1) !== youtubeVideos.length) {
        Slider.addNewPage();
        indexContainer += 1;
      }
    });
    Slider.addEvents(loadMoreFn);
    Slider.currentSlide(sliderPosition);
  }

  static adaptiveSlider(maxElem, youtubeVideoArray) {
    this.appendCards(youtubeVideoArray, maxElem, loadMoreFn);
  }
}


export default Cards;
