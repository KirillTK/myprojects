import YouTube from './youtube';

const html = '<div class="container">\n'
  + '  <h1>YouTube client</h1>\n'
  + '  <form>\n'
  + '    <div class="search search-input">\n'
  + '      <div class="smart-search"><input type="text" class="search" id="input" placeholder="&nbsp;"><i\n'
  + '              class="fas fa-search"></i>\n'
  + '      </div>\n'
  + '      <span class="border"></span>\n'
  + '    </div>\n'
  + '  </form>\n'
  + '\n'
  + '  <div class="cards">\n'
  + '  </div>\n'
  + '</div>';

function generateInitialHtml() {
  document.querySelector('body').innerHTML = html;
  document.querySelector('.fa-search').onclick = YouTube.findVideos;
}

export default generateInitialHtml;
