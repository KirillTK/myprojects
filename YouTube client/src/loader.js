const loaderHtml = '<div class="loader-container">\n'
  + '    <div class="loader"></div>\n'
  + '</div>';


class Loader {
  static inserLoader() {
    const cards = document.getElementsByClassName('cards')[0];
    cards.innerHTML += loaderHtml;
  }
}

export default Loader;
