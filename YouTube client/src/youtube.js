import Slider from './slider';
import Cards from './cards';
import Loader from './loader';

let youtubeVideoArray = [];
let pageToken = '';

class YouTube {
  static getStatistics(videos) {
    const youtubeVideos = videos;
    let id = `${youtubeVideos[0].id}`;
    for (let i = 1; i < youtubeVideos.length; i += 1) {
      id += `,${youtubeVideos[i].id}`;
    }
    fetch(`https://www.googleapis.com/youtube/v3/videos?part=statistics&id=${id}&key=AIzaSyDQ-wrmzyQBIeWx8rcFB1CLrEYH4WY3bfs`)
      .then(response => response.json())
      .then((data) => {
        data.items.forEach((value, position) => {
          youtubeVideos[position].viewCount = value.statistics.viewCount;
        });
        youtubeVideoArray = youtubeVideoArray.concat(youtubeVideos);
        Cards.appendCards(youtubeVideoArray, Slider.getMaxCountElements(), this.loadMore);
      });
  }

  static findVideos() {
    youtubeVideoArray = [];
    Slider.clearHtml();
    Slider.generateSlider();
    Loader.inserLoader();
    const q = document.getElementById('input').value;
    const videos = [];
    fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=15&q=${q}&type=video&videoCaption=closedCaption&key=AIzaSyDQ-wrmzyQBIeWx8rcFB1CLrEYH4WY3bfs`)
      .then(response => response.json())
      .then((data) => {
        pageToken = data.nextPageToken;
        data.items.forEach((value) => {
          const video = YouTube.getVideoInformation(value);
          videos.push(video);
        });
        YouTube.getStatistics(videos);
      })
      .catch(error => error);
  }

  static getVideoInformation(value) {
    const date = this.convertDate(value.snippet.publishedAt);
    return {
      id: value.id.videoId,
      channelTitle: value.snippet.channelTitle,
      description: value.snippet.description,
      publishedAt: date,
      title: value.snippet.title,
      image: value.snippet.thumbnails,
    };
  }

  static loadMore() {
    const q = document.getElementById('input').value;
    fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=15&pageToken=${pageToken}&q=${q}&type=video&videoCaption=closedCaption&key=AIzaSyDQ-wrmzyQBIeWx8rcFB1CLrEYH4WY3bfs`)
      .then(response => response.json())
      .then((data) => {
        pageToken = data.nextPageToken;
        const videos = [];
        data.items.forEach((value) => {
          const video = YouTube.getVideoInformation(value);
          videos.push(video);
        });
        YouTube.getStatistics(videos);
      })
      .catch(error => error);
  }

  static convertDate(date) {
    return date.match(/\d{4}-\d{2}-\d{2}/gm)[0];
  }
}

window.addEventListener('resize', () => {
  const div = document.querySelector('.cards');
  if (div.childElementCount > 0) {
    const countElements = Math.floor(div.clientWidth / 400);
    if (countElements <= 4 && countElements !== 0) {
      setInterval(Cards.adaptiveSlider(countElements, youtubeVideoArray), 1000);
    }
    if (countElements === 0) {
      setInterval(Cards.adaptiveSlider(1, youtubeVideoArray), 1000);
    }
  }
}, false);


export default YouTube;
